[#intro]
== Introducción

El objetivo de esta guía es proporcionar distintas soluciones a los desafíos del teletrabajo. Se tenderá a la utilización de soluciones basadas en software libre o código abierto.

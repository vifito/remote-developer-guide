== Almacenamiento de ficheros en la nube

=== Rclone

[cols="^1,3",frame=none,grid=none]
|===
|image:./images/rclone-logo.png[RClone,150,145]
|link:https://rclone.org/[Rclone] es una utilidad de línea de comandos que permite sincronizar ficheros y directorios desde los principales sistemas de almacenamiento en la nube: Dropbox, Google Drive, OneDrive, NextCloud, Webdav, FTP, Amazon S3, DigitalOcean Spaces, ...

|===

Las herramientas personales más utilizadas como Dropbox, Google Drive o OneDrive, cuentan con clientes de escritorio que permiten sincronizar los ficheros de trabajo en un directorio que se configure. En el caso de escritorios GNU/Linux no siempre existen clientes para todas estas herramientas pero con **Rclone** disponemos de una herramienta que se puede configurar con múltiples cuentas y trabajar con distintas soluciones. Rclone sería la `navaja suiza` para trabajar con las distintas soluciones que trabajan con ficheros en la nube.

==== Rclone GUI

Rclone GUI es una de las opciones que viene con rclone para proporcionar una interfaz gráfica, vía web, para rclone. Implementada con link:https://es.reactjs.org/[reactjs] y bajo licencia MIT. Desde la interfaz tendremos disponibles los distintos `remotes` que tenemos configurados con `rclone`.

WARNING: Las funcionalidades aún están en fase experimental.

.Rclone browser en funcionamiento
image::images/rclone-gui.png[] 

Para lanzar la interfaz gráfica executamos la siguiente instrucción desde línea de comandos.

[source,shell]
----
rclone rcd --rc-web-gui --rc-user=username --rc-pass=xxx --rc-web-gui-update
----

==== RcloneBrowser

Otra alternativa para disponer de una interfaz gráfica para rclone es link:https://github.com/kapitainsky/RcloneBrowser[RcloneBrowser]. Está disponible en formato AppImage, exe y dmg para los sistemas operativos macOS, GNU/Linux, BSD y Windows, en el apartado de link:https://github.com/kapitainsky/RcloneBrowser/releases[releases de Github].

.Remote configurado en rclone browser
image::images/rclone-browser-02.png[] 

.Navegando por los ficheros de un remote
image::images/rclone-browser-01.png[] 

Puede automatizarse su descarga con los siguientes comandos:

[source,shell]
----
wget -c https://github.com/kapitainsky/RcloneBrowser/releases/download/1.8.0/rclone-browser-1.8.0-a0b66c6-linux-i386.AppImage
chmod +x rclone-browser-1.8.0-a0b66c6-linux-x86_64.AppImage
----

////
=== Nextcloud
////

[#scm]
== Sistemas de Control de Versiones

El control de versiones en los proyectos software es fundamental. Seguramente ya estés usando un sistema de control de versiones, ya sea subversion, git u otros. Si el sistema a emplear en la organización viene determinado por la propia empresa poco podemos hacer.

A continuación hablaremos de soluciones basadas en Git, un software de control de versiones distribuído y que nos permite trabajar con distintas configuraciones de nuestro entorno. Podremos trabajar en nuestro equipo con Git y enviar los cambios a un sistema de control de versiones centralizado como subversion.

////
Git es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora y coordinar el trabajo que varias personas realizan sobre archivos compartidos. 
////

TIP: Visual Studio Code viene con link:https://code.visualstudio.com/docs/editor/versioncontrol[integración con git]. Sus funcionalidades que pueden ser ampliadas usando extensiones como link:https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens[GitLens].

////
=== Configurando Git en un servidor

https://git-scm.com/book/es/v2/Git-en-el-Servidor-Configurando-Git-en-un-servidor
////

=== RabbitVCS, cliente gráfico para Subversion y Git

image::images/rabbitvcs-logo.png[] 

[source,shell]
----
sudo add-apt-repository ppa:rabbitvcs/ppa
sudo apt update 

# for the Nautilus
sudo apt install rabbitvcs-nautilus3 

# for the Thunar extensions
sudo apt install rabbitvcs-thunar 

# for the GEdit extension
sudo apt install rabbitvcs-gedit 

# for the command line launchers
sudo apt install rabbitvcs-cli 
----

=== Gitea

[cols="^1,3",frame=none,grid=none]
|===

| image:images/gitea-logo.png[Gitea,120,120]
| link:https://gitea.io/[Gitea], __Git with a cup of tea__, es una solución de alojamiento de código escrita en lenguaje link:https://golang.org/[Go] y bajo licencia MIT. Gitea es un __fork__ de links:https://gogs.io/[Googs]. En otras palabras, un software para disponer de un Github propio.

|===

Gitea proporciona una serie de características adicionales a las de un simple repositorio git. Si lo único que se necesita es versionar los ficheros de un proyecto podemos limitarnos a link:https://git-scm.com/book/es/v2/Git-en-el-Servidor-Configurando-Git-en-un-servidor[configurar git en un servidor]. Por el contrario, si estamos buscando una solución de alojamiento git del estilo de Github, Gitlab o Bitbucket, y queremos tener todo alojado en nuestros servidores, Gitea nos proporciona ese servicio. Uno de los eslogan de la página de inicio de Gitea es: "__Gitea has low minimal requirements and can run on an inexpensive Raspberry Pi.__", lo que da una medida de los pocos requisitos hardware necesarios para lanzar el servicio.

NOTE: Las soluciones de alojamiento git: link:https://github.com/[github], link:https://gitlab.com/[gitlab] y link:https://bitbucket.org/[bitbucket], también permiten repositorios privados. Si tu organización no impone ninguna limitación sobre donde alojar el código, y la legislación tampoco establece ninguna restricción pueden ser servicios muy útiles. Recuerda: «__Cuando un producto es gratis, el producto eres tú.__».

En la propia link:https://docs.gitea.io/en-us/[documentación del proyecto] se recogen distintas métodos de instalación. A continuación nos centramos en la link:https://docs.gitea.io/en-us/install-with-docker/[instalación con Docker]. Los requisitos para la instalación de Gitea con docker son tener docker instalado y configurado y la herramienta de línea de comandos link:https://docs.docker.com/compose/[Docker Compose].

// TODO: documentar la instalación de docker y docker-compose
// https://github.com/docker/compose/releases

La instalación más simple requiere crear un directorio con nombre `gitea` que será donde se guarden nuestros datos. En el directorio que contiene la carpeta `gitea` se crea el fichero `docker-compose.yml`, cuyo contenido se detalla a continuación, y que se puede link:https://vifito.gitlab.io/remote-developer-guide/files/gitea/docker-compose.yml[descargar] desde el repositorio de esta guía. 

TIP: Otras configuraciones avanzadas pueden requerir el uso de base de datos MySQL, PostgreSQL, ... que se detallan en la documentación oficial. 

.Fichero docker-compose.yml para lanzar Gitea
[source,yaml]
----
version: "2"

networks:
  gitea:
    external: false

services:
  server:
    image: gitea/gitea:latest
    environment:
      - USER_UID=1000
      - USER_GID=1000
    restart: always
    networks:
      - gitea
    volumes:
      - ./gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
----

Para lanzar el contenedor con Gitea ejecutamos la siguiente sentencia:

[source,shell]
----
# Para lanzar el contenedor con gitea
docker-compose up
----

Una vez lanzado el contenedor estará disponible el puerto `222` para acceso por `SSH` y poder trabajar con git. En `http://localhost:3000` tendremos la interfaz web, un diseño muy similar al que utiliza Github.

.Pantallazo de un proyecto en Gitea
image::images/gitea-web.png[]

=== Otras soluciones

* Gogs: https://gogs.io/